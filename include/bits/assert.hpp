/* Copyright (C) 2018 INRA
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef ORG_VLEPROJECT_BITS_ASSERT_HPP
#define ORG_VLEPROJECT_BITS_ASSERT_HPP

#include <exception>

#include <cstdio>

#define stringify_detail(x) #x
#define stringify(x) stringify_detail(x)

#if defined(__clang__) || defined(__GNUC__)
#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)
#else
#define likely(x) (!!(x))
#define unlikely(x) (!!(x))
#endif

//
// In internal API, we prefer abort application when precondition,
// postcondition or assertion fail. For external API, user can select to use
// exception or return with message in terminal.
//

namespace bits {
namespace details {

inline void
print(const char* type,
      const char* cond,
      const char* file,
      const char* line) noexcept
{
    std::fprintf(
      stderr, "%s [%s] failure at %s: %s\n", type, cond, file, line);
}

[[noreturn]] inline auto
fail_fast(const char* type,
          const char* cond,
          const char* file,
          const char* line) -> void
{
    print(type, cond, file, line);
    std::terminate();
}

} // namespace details
} // namespace bits

#define BITS_CONTRACT_CHECK(type, cond)                                       \
    (likely(cond) ? static_cast<void>(0)                                      \
                  : bits::details::fail_fast(                                 \
                      type, stringify(cond), __FILE__, stringify(__LINE__)))

#define BITS_CONTRACT_CHECK_RETURN_VAL(type, cond, val)                       \
    do {                                                                      \
        if (unlikely(!(cond))) {                                              \
            bits::details::print(                                             \
              type, stringify(cond), __FILE__, stringify(__LINE__));          \
            return val;                                                       \
        }                                                                     \
    } while (0)

#define BITS_CONTRACT_CHECK_RETURN(type, cond)                                \
    do {                                                                      \
        if (unlikely(!(cond))) {                                              \
            bits::details::print(                                             \
              type, stringify(cond), __FILE__, stringify(__LINE__));          \
            return;                                                           \
        }                                                                     \
    } while (0)

#ifdef NDEBUG
#define bits_expects(cond)
#define bits_ensures(cond)
#define bits_assert(cond)
#define bits_return_val_if_fail(cond, val)
#define bits_return_if_fail(cond)
#else
#define bits_expects(cond) BITS_CONTRACT_CHECK("Precondition", cond)
#define bits_ensures(cond) BITS_CONTRACT_CHECK("Postcondition", cond)
#define bits_assert(cond) BITS_CONTRACT_CHECK("Assertion", cond)

#define bits_return_val_if_fail(cond, val)                                    \
    BITS_CONTRACT_CHECK_RETURN_VAL("Precondition", cond, val)

#define bits_return_if_fail(cond)                                             \
    BITS_CONTRACT_CHECK_RETURN("Precondition", cond)
#endif

#endif
