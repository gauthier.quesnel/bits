/* Copyright (C) 2016-2018 INRA
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef ORG_VLEPROJECT_BITS_TAGGED_UNION_HPP
#define ORG_VLEPROJECT_BITS_TAGGED_UNION_HPP

#include <string>

namespace bits {

/**
 * @brief @c tagged_union is a simple class to represent a union between an
 *     integer, a real and a string (resp. @c int, @c double and @c
 *     std::string).
 *
 * @details Waiting for a compiler with the @c std::variant class, @c
 *     tagged_union implements an union between a @c int, a @c double and a @c
 *     std::string. @c tagged_union, by default, defines a @c int with the
 *     value of @c 0. Use the @c tagged_union::type function to get the current
 *     type stored into the @c tagged_union.
 */
class tagged_union
{
public:
    enum class tag
    {
        integer,
        real,
        string
    };

    union
    {
        int l;
        double d;
        std::string s;
    };

    tag type{ tag::integer };

    tagged_union()
      : l(0)
    {}

    tagged_union(int value_)
      : l(value_)
      , type(tag::integer)
    {}

    tagged_union(double value_)
      : d(value_)
      , type(tag::real)
    {}

    tagged_union(std::string value_)
      : s(std::move(value_))
      , type(tag::string)
    {}

    tagged_union(const char* value_)
      : s(value_)
      , type(tag::string)
    {}

    tagged_union(const tagged_union& w)
      : type(w.type)
    {
        switch (w.type) {
        case tag::integer:
            l = w.l;
            break;
        case tag::real:
            d = w.d;
            break;
        case tag::string:
            new (&s) std::string(w.s);
            break;
        }
    }

    tagged_union(tagged_union&& w)
      : type(w.type)
    {
        switch (w.type) {
        case tag::integer:
            l = w.l;
            break;
        case tag::real:
            d = w.d;
            break;
        case tag::string:
            new (&s) std::string(w.s);
            break;
        }

        w.type = tag::integer;
        w.l = 0;
    }

    tagged_union& operator=(const tagged_union& w)
    {
        if (type == tag::string and w.type == tag::string) {
            s = w.s;
            return *this;
        }

        if (type == tag::string) {
            using std::string;
            s.~string();
        }

        switch (w.type) {
        case tag::integer:
            l = w.l;
            break;
        case tag::real:
            d = w.d;
            break;
        case tag::string:
            new (&s) std::string(w.s);
            break;
        }

        type = w.type;
        return *this;
    }

    tagged_union& operator=(tagged_union&& w)
    {
        if (type == tag::string and w.type == tag::string) {
            new (&s) std::string(w.s);
            w.type = tag::integer;
            w.l = 0;
            return *this;
        }

        if (type == tag::string) {
            using std::string;
            s.~string();
        }

        switch (w.type) {
        case tag::integer:
            l = w.l;
            break;
        case tag::real:
            d = w.d;
            break;
        case tag::string:
            new (&s) std::string(w.s);
            break;
        }

        type = w.type;

        w.type = tag::integer;
        w.l = 0;

        return *this;
    }

    tagged_union& operator=(double value)
    {
        if (type == tag::real) {
            d = value;
            return *this;
        }

        if (type == tag::string) {
            using std::string;
            s.~string();
        }

        type = tag::real;
        d = value;

        return *this;
    }

    tagged_union& operator=(int value)
    {
        if (type == tag::integer) {
            l = value;
            return *this;
        }

        if (type == tag::string) {
            using std::string;
            s.~string();
        }

        type = tag::integer;
        l = value;

        return *this;
    }

    tagged_union& operator=(const std::string& value)
    {
        if (type == tag::string) {
            s = value;
            return *this;
        }

        if (type == tag::string) {
            using std::string;
            s.~string();
        }

        type = tag::string;
        new (&s) std::string(value);

        return *this;
    }

    tagged_union& operator=(const char* value)
    {
        if (type == tag::string) {
            s = value;
            return *this;
        }

        if (type == tag::string) {
            using std::string;
            s.~string();
        }

        type = tag::string;
        new (&s) std::string(value);

        return *this;
    }

    ~tagged_union() noexcept
    {
        if (type == tag::string) {
            using std::string;
            s.~string();
        }
    }

    void swap(tagged_union& p)
    {
        tagged_union copy(*this);
        *this = p;
        p = copy;
    }
};

} // namespace bits

#endif
