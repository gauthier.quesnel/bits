/* Copyright (C) 2018 INRA
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <tuple>
#include <vector>

#include <bits/assert.hpp>
#include <bits/getopt.hpp>
#include <bits/tagged-union.hpp>
#include <bits/unit-test.hpp>

static void
check_getopt()
{
    auto params = std::make_tuple(false, 5l);
    using tuple_type = decltype(params);

    std::vector<bits::option<tuple_type>> opts{
        { { "This help message",
            "help",
            "h",
            bits::toggle_parameter<tuple_type>() },
          { "Set verbosity level",
            "verbose",
            "v",
            bits::arithmetic_parameter<tuple_type, long, 0, 7>() } }
    };

    printf("Options:\n");
    for (const auto& elem : opts) {
        if (elem.long_argument() and elem.short_argument())
            printf("--%s -%s", elem.long_argument(), elem.short_argument());
        else if (elem.long_argument())
            printf("--%s", elem.long_argument());
        else
            printf("-%s", elem.short_argument());

        if (elem.description())
            printf("\t%s\n", elem.description());
    }

    {
        int optind;
        int argc = 3;
        const char* argv[] = { "prog", "--help", "-v:4" };
        auto result =
          bits::getopt<tuple_type>(argc, argv, opts, params, optind);
        Ensures(result == bits::getopt_status::done);
        Ensures(bits::runtime_get<bool>(params, 0) == true);
        Ensures(bits::runtime_get<long>(params, 1) == 4l);
        Ensures(std::get<0>(params) == true);
        Ensures(std::get<1>(params) == 4);
    }
    {
        int optind;
        int argc = 3;
        const char* argv[] = { "prog", "-h", "-v=4" };
        auto result =
          bits::getopt<tuple_type>(argc, argv, opts, params, optind);
        Ensures(result == bits::getopt_status::done);
        Ensures(bits::runtime_get<bool>(params, 0) == false);
        Ensures(bits::runtime_get<long>(params, 1) == 4l);
        Ensures(std::get<0>(params) == false);
        Ensures(std::get<1>(params) == 4);
    }
    {
        int optind;
        int argc = 4;
        const char* argv[] = { "prog", "--help", "-v", "4" };
        auto result =
          bits::getopt<tuple_type>(argc, argv, opts, params, optind);
        Ensures(result == bits::getopt_status::done);
        Ensures(bits::runtime_get<bool>(params, 0) == true);
        Ensures(bits::runtime_get<long>(params, 1) == 4l);
        Ensures(std::get<0>(params) == true);
        Ensures(std::get<1>(params) == 4);
    }
    {
        auto ret = std::make_tuple(false, false, false, false, 1u);
        using real_tuple = decltype(ret);

        std::vector<bits::option<real_tuple>> opts2{
            { { "This help message",
                "help",
                "h",
                bits::exist_parameter<real_tuple>() },
              { "Evaluation mode",
                "evaluate",
                "e",
                bits::exist_parameter<real_tuple>() },
              { "Optimization mode",
                "optimize",
                "o",
                bits::exist_parameter<real_tuple>() },
              { "Prediction mode",
                "predict",
                "p",
                bits::exist_parameter<real_tuple>() },
              { "Parallel jobs (threads)",
                "jobs",
                "j",
                bits::arithmetic_parameter<
                  real_tuple,
                  unsigned,
                  1u,
                  std::numeric_limits<unsigned>::max()>() } }
        };

        int optind;
        int argc = 6;
        const char* argv[] = { "prog", "-e",    "--optimize",
                               "-j:4", "file1", "file2" };
        auto result = bits::getopt<real_tuple>(argc, argv, opts2, ret, optind);
        Ensures(result == bits::getopt_status::done);

        for (int i = optind; i != argc; ++i)
            printf("[%s] ", argv[i]);
        printf("\n");
    }
}

static void
check_tagged_union()
{
    bits::tagged_union real{ 3.0 };
    Ensures(real.type == bits::tagged_union::tag::real);

    bits::tagged_union integer{ 1000 };
    Ensures(integer.type == bits::tagged_union::tag::integer);

    bits::tagged_union str{ "hello world" };
    Ensures(str.type == bits::tagged_union::tag::string);

    str = real;
    Ensures(str.type == bits::tagged_union::tag::real);
    Ensures(str.d == 3.0);

    str = integer;
    Ensures(str.type == bits::tagged_union::tag::integer);
    Ensures(str.l == 1000);

    std::vector<bits::tagged_union> x(100);
    for (auto& elem : x) {
        Ensures(elem.type == bits::tagged_union::tag::integer);
        Ensures(elem.l == 0);
    }

    auto y = bits::tagged_union(4.0);
    Ensures(y.type == bits::tagged_union::tag::real);
    Ensures(y.d == 4.0);

    x[0] = bits::tagged_union(5.0);
    Ensures(x[0].type == bits::tagged_union::tag::real);
    Ensures(x[0].d == 5.0);

    x[0].swap(x[1]);
    Ensures(x[0].type == bits::tagged_union::tag::integer);
    Ensures(x[0].l == 0l);
    Ensures(x[1].type == bits::tagged_union::tag::real);
    Ensures(x[1].d == 5.0);

    x[2] = std::move(x[1]);
    Ensures(x[0].type == bits::tagged_union::tag::integer);
    Ensures(x[0].l == 0l);
    Ensures(x[1].type == bits::tagged_union::tag::integer);
    Ensures(x[1].l == 0l);
    Ensures(x[2].type == bits::tagged_union::tag::real);
    EnsuresEqual(x[2].d, 5.0);
    EnsuresNotEqual(x[2].d, 6.0);

    x[3] = bits::tagged_union(std::string("hello world!"));
    Ensures(x[3].type == bits::tagged_union::tag::string);
    Ensures(x[3].s == "hello world!");
}

int
main(int argc, char const* /*argv*/ [])
{
    bits_expects(argc == 1);
    bits_ensures(argc == 1);
    bits_assert(argc == 1);

    bits_return_val_if_fail(argc == 1, EXIT_FAILURE);

    check_tagged_union();
    check_getopt();

    return unit_test::report_errors();
}
